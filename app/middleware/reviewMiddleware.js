const getAllReviewMidlleware = (req,res,next)=>{
    console.log("Get all Review ");
    next()
}

const getReviewMidlleware = (req,res,next)=>{
    console.log("Get a Review ");
    next()
}

const putReviewMidlleware = (req,res,next)=>{
    console.log("put Review ");
    next()
}

const postReviewMidlleware = (req,res,next)=>{
    console.log("post Review ");
    next()
}

const deleteReviewMidlleware = (req,res,next)=>{
    console.log("delete Review ");
    next()
}

module.exports = {
    getAllReviewMidlleware,
    getReviewMidlleware,
    postReviewMidlleware,
    putReviewMidlleware,
    deleteReviewMidlleware
}