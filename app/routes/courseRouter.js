//Khai báo thư viện express 
const express = require("express");

//Khai báo middleware
const {
    getAllCourseMidlleware,
    getCourseMidlleware,
    postCourseMidlleware,
    putCourseMidlleware,
    deleteCourseMidlleware
} = require (`../middleware/courseMiddleware`)

//Tạo Router
const courseRouter = express.Router();

// sử dụng router
courseRouter.get("/course",getAllCourseMidlleware,(req,res)=>{
    res.json({
        message: "get all course"
    })
});

courseRouter.get("/course/:courseId", getCourseMidlleware, (req,res)=>{
    let courseId = req.params.courseId
    res.json ({
        message:`courseId = ${courseId}`
    })
});

courseRouter.put("/course/:courseId", putCourseMidlleware, (req,res)=>{
    let courseId = req.params.courseId
    res.json ({
        message:`courseId = ${courseId}`
    })
});

courseRouter.post("/course", postCourseMidlleware, (req,res)=>{
    res.json({
        message: "post a course"
    })
});
 courseRouter.delete("/course/:courseId", deleteCourseMidlleware, (req,res)=>{
    let courseId = req.params.courseId
    res.json ({
        message:`courseId = ${courseId}`
    })
 });

 module.exports = {courseRouter};

