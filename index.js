const express = require("express");
const { courseRouter } = require("./app/routes/courseRouter");
const { reviewRouter } = require("./app/routes/reviewRouter");

const app = express();

const prot = 8000;

//Middeware application-level middleware
app.use((req,res,next)=>{
    console.log(new Date());

    next();
})

app.get("/",(req,res)=>{
    let day = new Date();
    console.log(`xin chào hôm nay là ngày ${day.getDate()} tháng ${day.getMonth()+1} năm ${day.getFullYear()}`)
    res.json({
        message: `xin chào hôm nay là ngày ${day.getDate()} tháng ${day.getMonth()+1} năm ${day.getFullYear()}`
    })
})

app.post("/",(req,res)=>{
    console.log("Post Method");
    res.status(200).json({
        message: "post method"
    })
})
// sử dụng router
app.use("/", courseRouter);
app.use("/",reviewRouter)

app.listen(prot, ()=>{
    console.log("app listen on prot", prot)
});
